import { FilmeDetalhePage } from './../../pages/filme-detalhe/filme-detalhe';
import { ModalController } from 'ionic-angular';
import { Component, Input } from '@angular/core';

/**
 * Generated class for the ListaFilmeComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'lista-filme',
  templateUrl: 'lista-filme.html'
})
export class ListaFilmeComponent {

  @Input()
  filmes: any;

  text: string;

  constructor(private _modalCtrl: ModalController) {
    console.log('Hello ListaFilmeComponent Component');
    this.filmes = [];
  }


  openFilmDetails(imdbID: number) {
    const modal = this._modalCtrl.create(FilmeDetalhePage, { imdbID });
    modal.present();
  }

}
