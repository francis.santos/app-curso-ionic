import { Component } from '@angular/core';

/**
 * Generated class for the SearchFilmeComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'search-filme',
  templateUrl: 'search-filme.html'
})
export class SearchFilmeComponent {

  text: string;

  constructor() {
    console.log('Hello SearchFilmeComponent Component');
    this.text = 'Hello World';
  }

  ionViewDidLoad(){
    console.log("Custom component");
  }

}
