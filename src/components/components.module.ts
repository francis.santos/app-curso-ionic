import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SearchFilmeComponent } from './search-filme/search-filme';
import { ListaFilmeComponent } from './lista-filme/lista-filme';
import { CommonModule } from '@angular/common';
import { IonicModule } from 'ionic-angular';

@NgModule({
	declarations: [
    SearchFilmeComponent,
    ListaFilmeComponent
  ],
	imports: [
    CommonModule,
    IonicModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
	exports: [SearchFilmeComponent,
    ListaFilmeComponent]
})
export class ComponentsModule {}
