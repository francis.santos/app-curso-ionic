import { BaseService } from './../base/base.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Filme } from './filme.model';

@Injectable()
export class FilmeService extends BaseService<Filme> {

  constructor(public http: HttpClient) {
    super(http);
  }

}
