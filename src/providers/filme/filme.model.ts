import { Model } from "../base/base.model";

export abstract class Filme extends Model {
  Title?: any;
  Poster?: any;
  Production?: any;
  Genre?: any;
  Actors?: any;
  Runtime?: any;
  imdbRating?: any;
  Director?: any;

  constructor(obj: Filme) {
    super(obj);
  }
}
