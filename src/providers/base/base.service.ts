import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';

import { Model } from './base.model';

// extensão do chrome para resolver problema com o CORS
// https://chrome.google.com/webstore/detail/cors-toggle/jioikioepegflmdnbocfhgmpmopmjkim
// desabilitando segunrança do chrome, executar no terminal: google-chrome --disable-web-security
export class BaseService<T extends Model> {

  endPoint: string;
  apikey: string = 'b31d7be6';

  //Adicionar segundo parametro no controller e validar
  constructor(protected http: HttpClient, domain: string = null) {
    if (domain) {
      this.endPoint = 'http://192.168.0.102:5000/api/v1/' + domain;
    } else {
      this.endPoint = 'http://www.omdbapi.com';
    }
  }

  get(filters: any): Observable<T> {
    filters.apikey = this.apikey;
    return this.http.get<T>(this.endPoint, { params: this.mapFilters(filters) });
  }

  save(data: T): Observable<T> {
    let stream: Observable<T>;

    // tslint:disable-next-line:no-null-keyword
    if (data.id === undefined || data.id === null) {
      stream = this.http.post<T>(`${this.endPoint}`, data);
    } else {
      stream = this.http.put<T>(`${this.endPoint}/${data.id}`, data);
    }

    return stream;
  }

  //Setar um valor default para o filtro
  query(filters: any = null): Observable<any> {
    //Adicionar a validação do filtro
    if (filters) filters.apikey = this.apikey;
    return this.http.get<any>(this.endPoint, { params: this.mapFilters(filters) });
  }


  //Adicionado novo método para realizar post genéricos
  post(subroute: string, body: any, reqOpts?: any):Observable<ArrayBuffer> {
    if (subroute && subroute !== '') { subroute = `/${subroute}`; };
    return this.http.post(this.endPoint + subroute, body, reqOpts);
  }

  private mapFilters(filters: any[]): HttpParams {
    let params = new HttpParams();

    for (let filter in filters) {
      params = params.append(filter, filters[filter]);
    }

    return params;
  }

}
