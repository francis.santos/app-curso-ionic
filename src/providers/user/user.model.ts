import { Model } from '../base/base.model';

export class User extends Model {

  public login?: string;
  public email?: string;
  public name?: string;
  public password?: string;
  public roles?: any;

  constructor(obj: any) {
    super(obj);
    this.id = obj.id;
    this.login = obj.login;
    this.password = obj.password;
    this.roles = obj.roles;
  }

}
