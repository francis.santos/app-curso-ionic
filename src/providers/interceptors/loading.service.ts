export class LoadingService {
  static showLoading: boolean = true;
  static showToast: boolean = true;
}
