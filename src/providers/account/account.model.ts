import { Model } from '../base/base.model';

export class Account extends Model {

  public login?: string;
  public email?: string;
  public password?: string;
  public user?: any;
  public token?: string;

  constructor(obj: any) {
    super(obj);
    this.id = obj.id;
    this.login = obj.login;
    this.email = obj.email;
    this.password = obj.password;
    this.user = obj.user;
    this.token = obj.token;
  }

}
