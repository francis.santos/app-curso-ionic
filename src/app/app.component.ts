import { AccountService } from './../providers/account/account.service';
import { FilmesPage } from './../pages/filmes/filmes';
import { Platform, Events, Nav } from 'ionic-angular';
import { Component, ViewChild } from '@angular/core';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  //Adicinando a página como string
  rootPage:any = 'Login';

  //Adicionar o event no construtor
  constructor(platform: Platform, statusBar: StatusBar,
    splashScreen: SplashScreen,
    private _event: Events,
    private _accountService: AccountService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      //Implementar um evento de logout
      this._event.subscribe('logout', () => {
        this._accountService.logoutAccount();
        this._event.unsubscribe('logout');
        this.nav.popToRoot();
      });

    });
  }
}

