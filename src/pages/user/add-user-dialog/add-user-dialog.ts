import { UserService } from './../../../providers/user/user.service';
import { ViewController, AlertController } from 'ionic-angular';
import { User } from './../../../providers/user/user.model';
import { Component } from '@angular/core';

/**
 * Generated class for the AddUserDialogComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'add-user-dialog',
  templateUrl: 'add-user-dialog.html'
})
export class AddUserDialogComponent {

  user: User;

  constructor(private _viewCtrl: ViewController,
              private _alertCtrl: AlertController,
              private _userService: UserService) {
    console.log('Hello AddUserDialogComponent Component');
    this.user = new User({
      name: '',
      email: ''
    });
  }

  onSave() {
    this._userService.save(this.user).subscribe(data => {
      console.log(data);
      const alert = this._alertCtrl.create({
        title: 'Cadastro realizado com sucesso!',
        message: `Um novo usuário para ${data.name.toUpperCase()} foi cadastrado com sucesso.`,
        buttons: [
          {
            text: 'Ok',
            role: 'cancel',
            handler: () => {
              this.close('cadastrado');
            }
          }
        ]
      });
      alert.present();
    });
  }

  close(params: any = null) {
    this._viewCtrl.dismiss(params);
  }
}
