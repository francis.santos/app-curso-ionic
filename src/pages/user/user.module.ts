import { AddUserDialogComponent } from './add-user-dialog/add-user-dialog';
import { UserService } from './../../providers/user/user.service';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserPage } from './user';

@NgModule({
  declarations: [
    UserPage,
    AddUserDialogComponent
  ],
  imports: [
    IonicPageModule.forChild(UserPage),
  ],
  entryComponents: [
    AddUserDialogComponent
  ],
  providers: [
    UserService
  ]
})
export class UserPageModule {}
