import { AddUserDialogComponent } from './add-user-dialog/add-user-dialog';
import { UserService } from './../../providers/user/user.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController } from 'ionic-angular';

/**
 * Generated class for the UserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {

  users: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private _event: Events,
    private _modalCtrl: ModalController,
    private _userService: UserService) {
    this.users = [];
  }

  ionViewWillEnter(){
    this.loadUsers();
  }

  loadUsers() {
    this._userService.query().subscribe(data => {
      console.log(data);
      this.users = data;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserPage');
  }

  add() {
    const modal = this._modalCtrl.create(AddUserDialogComponent);
    modal.present();
    modal.onDidDismiss(data => {
      if (data) {
        this.loadUsers();
      }
    });
  }

  logout() {
    this._event.publish('logout');
  }

}
