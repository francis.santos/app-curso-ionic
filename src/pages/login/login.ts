import { FilmesPage } from './../filmes/filmes';
import { Account } from './../../providers/account/account.model';
import { AccountService } from './../../providers/account/account.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'Login'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  account: Account;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private _accountService: AccountService) {
    this.account = new Account({
      email: '',
      password: ''
    });
  }

  ionViewWillEnter(){
    console.log('ionViewWillEnter LoginPage');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  onLogin() {
    this._accountService.authenticate(this.account).subscribe(data => {
      console.log(data);
      if (data) {
        console.log('Autenticação realizada com sucesso!!!');
        //Push para constrolar o botão de voltar do dispositivo
        this.navCtrl.push(FilmesPage);
      } else {
        console.log('Falha na autenticação.');
      }
    });
  }

}
