import { AccountService } from './../../providers/account/account.service';
import { ComponentsModule } from './../../components/components.module';
import { FilmeDetalhePageModule } from './../filme-detalhe/filme-detalhe.module';
import { FilmeService } from './../../providers/filme/filme.service';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FilmesPage } from './filmes';

@NgModule({
  declarations: [
    FilmesPage,
  ],
  imports: [
    IonicPageModule.forChild(FilmesPage),
    FilmeDetalhePageModule,
    ComponentsModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [
    FilmeService,
    AccountService
  ]
})
export class FilmesPageModule {}
