import { UserPage } from './../user/user';
import { AccountService } from './../../providers/account/account.service';
import { FilmeDetalhePage } from './../filme-detalhe/filme-detalhe';
import { FilmeService } from './../../providers/filme/filme.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-filmes',
  templateUrl: 'filmes.html',
})
export class FilmesPage {

  searchQuery: string = '';
  filmes: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private _accountService: AccountService,
              private _filmeService: FilmeService) {
  }

  getFilms(ev: any) {
    // Reset items back to all of the items
    this.filmes = [];

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this._filmeService.query({ s: val}).subscribe(data => {
        if (data.Response === 'True') {
          this.filmes = data.Search;
        }
      });
    }
  }

  loggout() {
    this._accountService.logoutAccount();
    this.navCtrl.popToRoot();
  }

  //Metodo para o menu
  goToUsers() {
    this.navCtrl.push(UserPage);
  }


}
