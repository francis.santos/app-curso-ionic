import { FilmeService } from './../../providers/filme/filme.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Filme } from '../../providers/filme/filme.model';

/**
 * Generated class for the FilmeDetalhePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filme-detalhe',
  templateUrl: 'filme-detalhe.html',
})
export class FilmeDetalhePage {

  filme: Filme;
  imdbID: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private _viewCtrl: ViewController,
              private _filmeService: FilmeService) {
    this.imdbID = navParams.get('imdbID');
  }

  ionViewWillEnter(){
    this._filmeService.get({ i: this.imdbID }).subscribe(data => {
      this.filme = data;
    });
  }

  close() {
    this._viewCtrl.dismiss();
  }

}
